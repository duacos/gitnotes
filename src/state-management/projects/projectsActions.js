import {
  PROJECTS_SYNCHRONIZING,
  PROJECTS_SYNCHRONIZED,
  PROJECTS_SYNCHRONIZATION_ERROR,
  APPEND_PROJECTS
} from "./projectsActionTypes";
import { ProjectsApi } from "../../api/gitlab";

const USER_PROJECTS_KEYS = "USER_PROJECTS";

const _fetchAndSyncProjects = async (
  accessToken,
  overwriteExisting = true,
  currentPage = 1
) => {
  let synchronizedProjects = await ProjectsApi.getCurrentlyAuthenticatedUserProjectsAsync(
    accessToken,
    currentPage
  );

  try {
    let persistedProjects = JSON.parse(
      localStorage.getItem(USER_PROJECTS_KEYS)
    );

    if (persistedProjects) {
      // TODO: optimisation

      synchronizedProjects = [...synchronizedProjects].map(
        synchronizedProject => {
          // TODO: should be O(1)
          const match = persistedProjects.find(
            p => synchronizedProject.id === p.id
          );

          if (match) {
            return {
              ...synchronizedProject,
              enabled: match.enabled
            };
          } else {
            return synchronizedProject;
          }
        }
      );
    } else {
      persistedProjects = [];
    }

    if (overwriteExisting) {
      localStorage.setItem(
        USER_PROJECTS_KEYS,
        JSON.stringify(synchronizedProjects)
      );
    } else {
      // in case of loadMore
      localStorage.setItem(
        USER_PROJECTS_KEYS,
        JSON.stringify(persistedProjects.concat(synchronizedProjects))
      );
    }
  } catch (error) {
    if (overwriteExisting) localStorage.removeItem(USER_PROJECTS_KEYS);
    throw error; // Error should be handled in the action creator as well.
  }

  return synchronizedProjects;
};

const getCurrentAuthenticatedUserProjects = accessToken => {
  return async dispatch => {
    dispatch({ type: PROJECTS_SYNCHRONIZING });

    try {
      let synchronizedProjects = await _fetchAndSyncProjects(
        accessToken,
        true,
        1
      );
      dispatch({
        type: PROJECTS_SYNCHRONIZED,
        payload: {
          synchronizedProjects
        }
      });
    } catch (error) {
      dispatch({
        type: PROJECTS_SYNCHRONIZATION_ERROR,
        payload: {
          error
        }
      });
    }
  };
};

const loadMoreProjects = (accessToken, currentPage) => {
  return async dispatch => {
    dispatch({ type: PROJECTS_SYNCHRONIZING });

    try {
      let synchronizedProjects = await _fetchAndSyncProjects(
        accessToken,
        false,
        currentPage
      );
      dispatch({
        type: APPEND_PROJECTS,
        payload: {
          projects: synchronizedProjects
        }
      });
    } catch (error) {
      dispatch({
        type: PROJECTS_SYNCHRONIZATION_ERROR,
        payload: {
          error
        }
      });
    }
  };
};

const getSynchronizedProjects = () =>
  JSON.parse(localStorage.getItem(USER_PROJECTS_KEYS));

const saveSynchronizedProjects = projects =>
  localStorage.setItem(USER_PROJECTS_KEYS, JSON.stringify(projects));

const toggleProject = (synchronizedProjects, projectId) => {
  return synchronizedProjects.map(project => {
    if (project.id === projectId) {
      return {
        ...project,
        enabled: !project.enabled
      };
    } else {
      return project;
    }
  });
};

const toggleProjectEdition = projectId => {
  const synchronizedProjects = getSynchronizedProjects();

  if (synchronizedProjects) {
    const updatedProjects = toggleProject(synchronizedProjects, projectId);
    saveSynchronizedProjects(updatedProjects);

    return {
      type: PROJECTS_SYNCHRONIZED,
      payload: {
        synchronizedProjects: updatedProjects
      }
    };
  }
};

export {
  getCurrentAuthenticatedUserProjects,
  toggleProjectEdition,
  loadMoreProjects
};
