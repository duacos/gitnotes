import {
  PROJECTS_SYNCHRONIZING,
  PROJECTS_SYNCHRONIZED,
  APPEND_PROJECTS
} from "./projectsActionTypes";

const initialState = {
  synchronizedProjects: [],
  lastUpdateTime: undefined,
  synchronizing: false,
  currentPage: 1
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PROJECTS_SYNCHRONIZING: {
      return {
        ...state,
        synchronizing: true
      };
    }

    case PROJECTS_SYNCHRONIZED: {
      const { synchronizedProjects } = action.payload;
      return {
        synchronizedProjects,
        lastUpdateTime: new Date().toUTCString(),
        synchronizing: false,
        currentPage: 1
      };
    }

    case APPEND_PROJECTS: {
      const { projects } = action.payload;
      return {
        synchronizedProjects: state.synchronizedProjects.concat(projects),
        lastUpdateTime: new Date().toUTCString(),
        synchronizing: false,
        currentPage:
          projects && projects.length
            ? state.currentPage + 1
            : state.currentPage
      };
    }

    default:
      return state;
  }
};
