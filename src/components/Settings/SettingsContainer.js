import Settings from "./Settings";
import {
  toggleProjectEdition,
  getCurrentAuthenticatedUserProjects,
  loadMoreProjects
} from "../../state-management/projects/projectsActions";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

const mapStateToProps = ({
  projects: { synchronizedProjects, lastUpdateTime, currentPage },
  authentication: { accessToken }
}) => {
  return {
    synchronizedProjects,
    lastUpdateTime,
    currentPage,
    accessToken
  };
};

const mapDispatchToProps = dispatch => ({
  toggleProjectEdition: projectId => dispatch(toggleProjectEdition(projectId)),
  getCurrentAuthenticatedUserProjects: () =>
    dispatch(getCurrentAuthenticatedUserProjects()),
  loadMoreProjects: (token, currentPage) =>
    dispatch(loadMoreProjects(token, currentPage))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Settings)
);
